#include <iostream>
using namespace std;
#include <ctime>
#include <string>
class copys
{
    public:
int aa;
char b;
};

class ar{
    
    copys *yo;
    int **a, rows, columns;

    const int array_num;
   
    public:
    static int objectCount;

    void change()
    {
       yo->aa = 123;
       yo->b = 'c';
    }
    
    ar(int rows = 0):array_num(1)
    {

    }
    ar(int r, int c, int d, copys *e):array_num(d) // constructor
    {
       yo = e;
        rows = r;
        columns = c;
        a = new int*[rows];

        for (int outer = 0; outer < rows; outer++)
             {a[outer] = new int[columns];}

        objectCount++;
    } 
    
    ar():array_num(1) // constructor
                      // set 1 as value for const int array_num
    {
       
        rows = 2;
        columns = 3;
        a = new int*[rows];

        for (int outer = 0; outer < rows; outer++)
             {a[outer] = new int[columns];}

        for (int outer = 0; outer < rows; outer++)
        {
            for(int inner = 0; inner < columns; inner++)
            {
                a[outer][inner] = 0;
            }
        }     

        objectCount++;
    }  
    

   ar(const ar &m):array_num(m.array_num)  // copy constructor
   {   // data members  are rows, columns, int **a(2d array) and const int array_num
       // last data member is static int objectCount
		rows = m.rows;
		columns = m.columns;
        
		a = new int*[rows];
        for (int outer = 0; outer < rows; outer++)
             {a[outer] = new int[columns];}

		for (int outer = 0; outer < rows; outer++)
       {
        for (int inner = 0; inner < columns; inner++)
             {
              a[outer][inner] = m.a[outer][inner];
             } 
       }      
		objectCount++;
	}
/*
   void  operator = (const ar &m) // assignment, assuming both arrays to have same dimensions

	{	
	    
	    rows = m.rows;
		columns = m.columns;
		
		for (int outer = 0; outer < rows; outer++)
       {
        for (int inner = 0; inner < columns; inner++)
             {
              a[outer][inner] = m.a[outer][inner];
             } 
       }      
	
     
	}
    */
    ar operator = (const ar &m) // assignment, assuming both arrays to have same dimensions

	{	
	    
	    rows = m.rows;
		columns = m.columns;
		
		for (int outer = 0; outer < rows; outer++)
       {
        for (int inner = 0; inner < columns; inner++)
             {
              a[outer][inner] = m.a[outer][inner];
             } 
       }      
	
     return *this;
	}
    ~ar() //destructor
    {
		freeMemory();	
        objectCount--;
	}
    void dec_ar()
    {
        
        
        for (int outer = 0; outer < rows; outer++)
       {
        for (int inner = 0; inner < columns; inner++)
             {
              a[outer][inner] = rand() % 10;
             } 
       }      
    }
    void freeMemory() // frees array
    {
		
		for (int inner = 0; inner < rows; inner++)
			delete []a[inner];
		delete []a;		
	}
    
    

    void show() // outputs out array
    {
       for (int outer = 0; outer < rows; outer++)
        {
            for(int inner = 0; inner < columns; inner++)
            {
               cout <<  a[outer][inner] << " ";
            }
            cout <<'\n';
        } 
        cout << ".............................." << '\n';
    }
    void ret_array_num()
    {
        cout << array_num << '\n';
    }                  

    void increment()
    {
        for (int outer =0; outer < rows; outer++)
        {
            for(int inner = 0; inner < columns; inner++)
            {
              a[outer][inner]++;
            }
        }
    }
    
    ar operator ++  () // this executes when you want to do ++object
    {  
     for (int outer = 0; outer < rows; outer++)
    {
        for (int inner = 0; inner < columns; inner++)
        {
         a[outer][inner] += a[outer][inner]; 
        }
        
    }    
    return *this;
    }

    ar operator ++  (int) // this executes when you want to do object++
    {  
     for (int outer = 0; outer < rows; outer++)
    {
        for (int inner = 0; inner < columns; inner++)
        {
         a[outer][inner] += a[outer][inner]; 
        }
        
    }    
    return *this;
    }
      /*          
	ar operator *  (ar const &obj) // this is in form new_object = obj1 * obj2
    {  
     // ar new_ar; here constructor with no parameters is called  
     ar new_ar(2,3,3); // constructor with paramenters is called
     for (int outer = 0; outer < new_ar.rows; outer++)
     {
         for (int inner = 0; inner < new_ar.columns; inner++)
         {
             new_ar.a[outer][inner] = a[outer][inner] + obj.a[outer][inner];  // we can add any integer over here 
                                                                     // max parameters for operator overloading is 1             
         }
     }

     return new_ar;
    }
    */
    ar operator + (int ELEMENT)// performs object + integer
    {
      for (int outer = 0; outer < rows; outer++)
      {
          for (int inner = 0; inner < columns; inner++)
          {
              a[outer][inner] += ELEMENT;
          }
      }
     return *this;

    }	
    friend class reset;
   friend void disp(ar &obj);
};
//////////////////////////////////////////////////
class reset
{
    int a;
    public:
    void set_0(ar &obj)
    {
     for (int outer = 0; outer < obj.rows; outer++)
     {
         for (int inner = 0; inner < obj.columns; inner++)
         {
             obj.a[outer][inner] = 0;
         }
     }

    }

};



// note ar is friend of reset, but reset is not friend of ar 
//so ar can't modify resets data values but reset can
void disp(ar &obj) // object passed byref
{
   for (int outer = 0; outer < obj.rows; outer++)
     {
         for (int inner = 0; inner < obj.columns; inner++)
         {
             obj.a[outer][inner] = 0;
         }
     }
}
int ar::objectCount = 0;
/*
void yo()
{
    ar::objectCount++;
    cout << ar::objectCount++; // value changed byval

}
*/
ostream& operator<<(ostream &out, ar &array)
{
	out << "Array objects  are  : \n" << ar::objectCount;
	return out;
}	// don't use this type of overloading with functions with type void
    // but only with functions which return values

int main()
{   // ar *array = new ar[10]; // dynamic
    //ar *array = new ar[10]{ar(1,2,3123), ar(1,2,4)}; // dynamic
    //array[0].dec_ar();  // dynamic
    //array[0].ret_array_num();  // dynamic
    
    //ar b[10] = {ar(1,2,3), ar(1,2,1231)}; // static
    //b[0].ret_array_num(); // static


    copys bb;
   
    ar a(2,3,1, &bb);
    a.dec_ar();
    a.change();
    cout << bb.aa;
  //  a.show();
   // disp(a); // friend of class ar
   // a.show();
    

/*
    srand(time(0));

    ar a(2,3,1);
    a.dec_ar();
    ar b = a;
    a.show();
    b.show();
    ar c(2, 3, 5);
    c.ret_array_num();
  */  
    //reset b;
   // b.set_0(a);
   //disp(a);
   //a.show();
  
    //cout << ar::objectCount;    
    // cout << a; ostream& operator executed here
   // ar b(2, 3, 2);



    return 0;
}


