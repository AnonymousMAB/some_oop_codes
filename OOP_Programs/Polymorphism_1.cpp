#include <iostream>
using namespace std;
#include <ctime>
#include <string>
// a child class will call constructors and destructors of parent class by default
// you can override which type of constructor gets called in child class's constructor definition
// in case of multiple inheritances order of constructor and destructor will be what is at declaration
// that order can't be changed

// for classes in hiearchy child class calls parent class cons/des which in turn call their parent class cons
// the higher the class the earlier its constructor gets called and the later its destuctor gets called

// note abstract and non-pure abstract classes allow inheritance and accessing of data members


         
// 2 sibling classes can't have same named functions no matter what 
// but if you manipulate in way you'll  call them then sibling classes can have same named functions
// and the ultimate parent can only be allowed to have a function if and only if inheritance is virtual          


// however sibling classes can have functions not in parent and the other sibling classes
class start 
{
    
 public:
 int aa;
 start (int a)
 {
    cout << "start parametrized constructor called \n";
 }
 start()
 {
     cout << "start constructor called \n"; 
 }
 ~start()
 {
   cout << "start destructor called \n";    
 }
 void fl()
 {
     cout << "fl called \n";
 }

  void xy()
  {
      cout << "xy of start called \n";
  }
  
};

///////////////////////////////
class a: virtual public start 
{
    public:
 
    void xy()
    {
        cout << "xy of a\n";
    }

a (int a)
{
    cout << "a parametrized constructor called \n";
} 
a()
{
    cout << "a constructor called \n";
    //fl();
}

~a()
{
    cout << "a destructor called \n";
}
};

///////////////////////////
class b: virtual public start 
{
    public:

b (int a)
{
    cout << "b parametrized constructor called \n";
} 
b()
{
 cout << "b constructor called \n";
 
}
void yo()
{
    cout << "yo called \n";
}
/*
void xy(int z)
    {
        cout << "xy of b \n";
    }
*/  
~b()
{
 cout << "b destructor called \n";   
}
};

/////////////////////////////////
class c: public a, public b
{
    public:
   /*
    void xy()
    {
        cout << "xy of c\n";
    }
    */
c (int z):b(1), a(1), start(1)
{
    cout << "c parametrized constructor called \n";
} 
 c():b(1), a(1), start(1)
 {
     cout << "c constructor called\n";
   
 }
 ~c()
 {
     cout << "c destructor called \n";   
    a::aa = 2;
 }
};
int main()
{
    c C;
    C.xy();
    //C.fl();
    //C.start::xy();
   
    return 0;
}