#include <iostream>
using namespace std;
#include <ctime>
#include <string>


class start 
{
    
 public:
 int y;
 start()
 {
     cout << "start constructor called \n"; 
     y = rand() % 100;
 }
   ~start()
 {
   cout << "start destructor called \n";    
 }

 
 
 
  virtual void f() 
  {
      cout << "f of start called \n";
  }

  virtual void xy()
    {
        cout << "xy of start\n";
    }
    void elpa()
    {
        cout << "elpa of start called \n";
    }
};

class a:  virtual public start 
{
    public:
  
void ab()
 {
    cout << "ab of a called \n";
 }    
void z() 
{
    cout << "z of a called \n";
}
a()
{
    cout << "a constructor called \n";
    //f();
}
a(int z)
{
    cout << "a parameterized constructor called \n";
}
 ~a()
{
    cout << "a destructor called \n";
}

void xy()
    {
        cout << "xy of a\n";
    }
   
 
};

///////////////////////////////////////////////////////////////////////
class b:  virtual public start 
{
    public:
b()
{
 cout << "b constructor called \n";
 
}
/*
void xy()
    {
        cout << "xy of b \n";
    }
*/

virtual ~b()
{
 cout << "b destructor called \n";   
}
void xx()
{
    cout << "xx of  b called \n";
}


 void f() 
  {
      cout << "f of b called \n";
  }
};

/////////////////////////////////////////////////////////////////////
class c: public a, public b
{
    public:
 
 void ab()
 {
    cout << "ab of c called \n";
 } 
 
 void yo()
 {
     cout << "yo of c called \n";
 }
   
   void xy()
    {
        cout << "xy of c\n";
    }
  

 c()
 {
     cout << "c constructor called\n";
    
 }
 ~c()
 {
     cout << "c destructor called \n";   
    
 }
 
  void f() 
  {
      cout << "f of c called \n";
  }
};

int main()
{

    b *aa = new c; // b's destructor is declared virtual 
    /*
  b *aa; 
  c obj;
  aa = &obj;
  cout << aa->y << '\n';
  aa->f();
  aa->elpa();
  */

                        // we can constrict pointer between start and c but only the virtal functions are looked for       
            
                        // compiler only goes down from parent class pointer to look for functions declared virtual

                        // code won't compile without error if sibling classes inheritance of parent not declared virtual 

                        // compiler goes from lowest class and above to look for virtual function
   
  
  
 delete aa;
 
   // if ultimate parent pointer destructor is virtual, then deleting any pointer calls destructors of all classes
   // but if it's not virtual then only parent pointer destructors and above get called 

    
    return 0;
}

