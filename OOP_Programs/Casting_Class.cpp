#include <iostream>
#include <cstdlib>
#include <ctime>
#include <exception>
using namespace std;
 // edt
using namespace std;

class P1{
	public:
		virtual void f1(){	cout << "Function of P1\n";}
		
};
class P21:public P1{
	public:
		void f21(){	cout << "Function of P21\n";}
		void f1(){	cout << "Overrided Function of P21\n";}
};
class P31:public P21{
	public:
		void f31(){	cout << "Function of P31\n";}
		void f1(){	cout << "Overrided Function of P31\n";}
		//void a(){cout << "a of P1 called \n";}
};
class P41:public P31{
	public:
		void f41(){	cout << "Function of P41\n";}
		//void f1(){	cout << "Overrided Function of P41\n";}

};


class P{
	public:
		void f(P *p){	cout << "Function Called\n";	}
};
int main(){
    
    P *p11 = new P;
	const P *p2 = new P;
	//p1 -> f (p2);
	p11 -> f (const_cast<P*>(p2));
	// this const cast removes const part of p2 

	/*
	P1 *p1 = new P41;
	p1->f1();

	P21 *p21 = dynamic_cast<P21*>(p1);
	
	p21->f1();
    */
	P1 *p1 = new P41;
	p1->f1();
	P21 *p21 = static_cast<P21*>(p1);
	p21->f1();
    //p21->a();
    // the diiference is that static cast is unsafe and dynamic cast requires polymorphism 
	// what this dynamic cast essentially does is same as 
	//P21 *p21 = new P41;

	// what this static cast essentially does is same as 
	//P21 *p21 = new P41;

	//XYZ *pxyz = static_cast<XYZ*>(p1);
	//pxyz->fxyz();
	return 0;
}
